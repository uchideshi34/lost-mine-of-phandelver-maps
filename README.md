# Lost Mine of Phandelver Maps

This FoundryVTT module is compilation of maps for the Lost Mine of Phandelver adventure. It includes all maps for the main chapters.

All maps are designed for for [FoundryVTT](https://foundryvtt.com/) with walls, doors, lighting and windows set up. My knowledge of Foundry is limited so any errors or improvements please let me know. 

I make maps purely as a hobby so all of the maps are offered for free, but if you have money burning a hole in your pocket, these maps would not have been possible with the wonderful assets, in particular, the 2 Minute Tabletop ones, and the Dungeondraft software, so consider a Patreon subscription or purchase there.

# Credit

[2-Minute Tabletop](https://2minutetabletop.com/) - Ross's assets and maps are just wonderful and are the primary source of art for these maps.

[Crosshead Studios](https://crossheadstudios.com/) - while I don't use his work assets extensively (although it is great overall), I do use the Crosshead textures for grass and rock terrain.

[Dungeon Mapster](https://www.patreon.com/dungeonmapster/) - big fan of Ryan's work and it aligns very well with the 2MTT style. I used a scattering of assets from him.

Gogots - I used a scattering of assets made by gogots to fill in where there were no 2MTT ones available.

Other utility assets - additional lighting, shadows and clouds/dust effects came from Apprentice of Aule, Gnome Factory, Krager and Crave. Their work can be found on [Cartography Assets](https://cartographyassets.com/asset-category/specific-assets/dungeondraft/)

# More Maps

I make maps for the games I run and simply just for the fun of it. They are freely available to download [here on Google Drive](https://drive.google.com/drive/folders/1jZMoDOZBe-y5cRIGl8wRQVjeSnisZppN?usp=sharing). You can go ahead and use them without installing this module.

# Legal Bit

*uchideshi34's Lost Mine of Phandelver Map Pack is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.*

# Installation

Once you have installed and activated the module, the scenes will appear as in a compendium in your game.

```
https://gitlab.com/uchideshi34/lost-mine-of-phandelver-maps/-/raw/main/module.json
```

# Versions

v1.1.2 - Updated manifest to indicate support for FoundryVTT v11

v1.1.1 - Fixed typo in module path

v1.1.0 - Updated module format to FoundryVTT v10 format

v1.0.3 - Fixed a missing wall piece for Cragmaw Castle

v1.0.2 - Added a version of Wyvern Tor

v1.0.1 - Added alternate version of The Old Owl Well as well as a map for Ruins of Conyberry

v1.0.0 - Initial Baseline Release

[![Preview Image](zip/Preview_Page_LMOP.webp)](https://gitlab.com/uchideshi34/lost-mine-of-phandelver-maps/-/raw/main/zip/Preview_Page_LMOP.webp)




